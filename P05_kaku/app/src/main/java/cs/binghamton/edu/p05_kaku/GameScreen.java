package cs.binghamton.edu.p05_kaku;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cs.binghamton.edu.p05_kaku.util.GameOver;


public class GameScreen extends Activity {
    AnimatorSet animatorSet;
    AnimationSet animSet;
    ImageView fruitArray[] = new ImageView[5];
    ImageView bombs[] = new ImageView[2];
    int counter = 0;
    int temp = 0;
    int i1, i2;
    int score = 0;
    int delayPop = 0;
    int lives = 3;
    String scoreStr;
    String livesStr;
    private Timer mTimer;
    private TimerTask mTask;
    private View self;
    MediaPlayer mp;
    Display mdisp;
    Point mdispSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_screen);
        Typeface tf=Typeface.createFromAsset(getAssets(),"fonts/go3v2.ttf");
        TextView scr = (TextView)findViewById(R.id.scrDisplay);
        scr.setTypeface(tf);
        TextView life = (TextView)findViewById(R.id.textView);
        life.setTypeface(tf);
        TextView scrInt = (TextView)findViewById(R.id.score);
        scrInt.setTypeface(tf);
        TextView lifeInt = (TextView)findViewById(R.id.livesStr);
        lifeInt.setTypeface(tf);
        /*RotateAnimation anim = new RotateAnimation(0, 360, , );
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(5);
        anim.setDuration(1200);
        */
// Start animating the image
        mdisp = getWindowManager().getDefaultDisplay();
        mp = MediaPlayer.create(this, R.raw.home_bgm);
        mp.setLooping(true);
        mp.start();
        spawnFruits();
        spawnBombs();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void spawnFruits(){
        mdispSize = new Point();
        mdisp.getSize(mdispSize);
        int maxX = mdispSize.x;
        int maxY = mdispSize.y;
        for(int i=0; i<5; i++) {
            Random r = new Random();
            int i1 = r.nextInt((maxX-140) - 100) + 100;
            int i2 = r.nextInt((maxY-140) - 100) + 100;

            Random fruit_random = new Random();
            int rand = fruit_random.nextInt(7 -1) + 1;

            Random dRand = new Random();
            long delay = dRand.nextInt(1200 - 200) + 200;

            RelativeLayout rl = (RelativeLayout) findViewById(R.id.gameScreen);
            animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.fruit);
            fruitArray[i] = new ImageView(this);
            switch (rand){
                case 1:
                    fruitArray[i].setImageResource(R.drawable.banana);
                    break;
                case 2:
                    fruitArray[i].setImageResource(R.drawable.peach);
                    break;
                case 3:
                    fruitArray[i].setImageResource(R.drawable.apple);
                    break;
                case 4:
                    fruitArray[i].setImageResource(R.drawable.strawberry);
                    break;
                case 5:
                    fruitArray[i].setImageResource(R.drawable.tomato);
                    break;
                case 6:
                    fruitArray[i].setImageResource(R.drawable.passionfruit);
                default:
                    fruitArray[i].setImageResource(R.drawable.apple);
                    break;
            }

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(140, 140);
            fruitArray[i].setMinimumHeight(140);
            fruitArray[i].setMinimumWidth(140);
            fruitArray[i].setMaxHeight(140);
            fruitArray[i].setMaxWidth(140);
            params.leftMargin = i1;
            params.topMargin = maxY;
            fruitArray[i].setId(i);
            rl.addView(fruitArray[i], params);
            animatorSet.setTarget(fruitArray[i]);
            animatorSet.setStartDelay(delay);
            animatorSet.start();
            fruitArray[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int x = v.getId();
                    fruitArray[x].setVisibility(View.INVISIBLE);
                    //fruitArray[x] = null;
                    //Typeface tf=Typeface.createFromAsset(getAssets(),"fonts/go3v2.ttf");
                    score = score + 20;
                    scoreStr = String.valueOf(score);
                    TextView tv = (TextView) findViewById(R.id.score);
                    tv.setText(scoreStr);
                    spawnFruitAgain(x);
                    //spawnSingleFruit();
                }
            });

        }
    }

    private void spawnFruitAgain(int x){
        delayPop = x;
        fruitArray[x].postDelayed(new Runnable() {
            @Override
            public void run() {
                fruitArray[delayPop].setVisibility(View.VISIBLE);
            }
        }, 1400);
    }

    private void spawnBombAgain(int x){
        delayPop = x;
        bombs[x].postDelayed(new Runnable() {
            @Override
            public void run() {
                bombs[delayPop].setVisibility(View.VISIBLE);
            }
        }, 1400);
    }


    private void spawnBombs(){
        mdispSize = new Point();
        mdisp.getSize(mdispSize);
        int maxX = mdispSize.x;
        int maxY = mdispSize.y;
        for(int i=0; i<2; i++) {
            Random r = new Random();
            int i1 = r.nextInt((maxX-140) - 100) + 100;
            int i2 = r.nextInt((maxY-140) - 100) + 100;

            Random fruit_random = new Random();
            int rand = fruit_random.nextInt(5 -1) + 1;

            Random dRand = new Random();
            long delay = dRand.nextInt(1200 - 200) + 200;

            RelativeLayout rl = (RelativeLayout) findViewById(R.id.gameScreen);
            animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.fruit);
            bombs[i] = new ImageView(this);
            bombs[i].setImageResource(R.drawable.bomb);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(140, 140);
            bombs[i].setMinimumHeight(140);
            bombs[i].setMinimumWidth(140);
            bombs[i].setMaxHeight(140);
            bombs[i].setMaxWidth(140);
            params.leftMargin = i1;
            params.topMargin = maxY;
            bombs[i].setId(i);
            rl.addView(bombs[i], params);
            animatorSet.setTarget(bombs[i]);
            animatorSet.setStartDelay(delay);
            animatorSet.start();
            bombs[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int x = v.getId();
                    bombs[x].setVisibility(View.INVISIBLE);
                    //bombs[x] = null;
                    lives--;
                    if(lives == 0){
                        gameOver();
                    }
                    scoreStr = String.valueOf(score);
                    livesStr = String.valueOf(lives);
                    TextView tv = (TextView) findViewById(R.id.score);
                    TextView tv2 = (TextView) findViewById(R.id.livesStr);
                    tv.setText(scoreStr);
                    tv2.setText(livesStr);
                    spawnBombAgain(x);

                    //spawnSingleFruit();
                }
            });

        }
    }

    private void gameOver(){
        mp.stop();
        Intent intent = new Intent(this, GameOver.class);
        this.finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (mp.isPlaying()) {
            mp.stop();
        }
        mp.release();
        super.onBackPressed();
    }
}
